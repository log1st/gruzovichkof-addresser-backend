<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 14:48
	 */
	
	return [
		'singletons' => [
			\App\Src\Http\Router::class,
			\App\Src\Http\Request::class,
			\App\Src\Http\Response::class,
			\App\Src\Container\Contracts\Container::class,
		],
		'map' => [
			\App\Modules\Geo\Src\Contracts\GeoServiceContract::class => \App\Modules\Geo\Src\Services\YandexGeoService::class,
			\App\Modules\Geo\Src\Contracts\GeoItemsRepositoryContract::class => \App\Modules\Geo\Src\GeoItemsRepository::class,
			\App\Modules\Geo\Src\Contracts\GeoItemContract::class => \App\Modules\Geo\Src\GeoItem::class
		]
	];