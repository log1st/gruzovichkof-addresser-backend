<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 18.08.2018
	 * Time: 13:12
	 */
	
	namespace App\Src\Application;
	
	use App\Src\Container\Contracts\Container;
	use App\Src\Db\Connection;
	use App\Src\Http\Exceptions\HttpException;
	use App\Src\Http\Exceptions\UnknownRouteException;
	use App\Src\Http\Request;
	use App\Src\Http\Response;
	use App\Src\Http\Router;
	use ArgumentCountError;
	
	/**
	 * Interface ApplicationContract
	 * @package App\Src\Application\Contracts
	 */
	class Application
	{
		/**
		 * @var Application
		 */
		static $instance;
		/**
		 * @var Container
		 */
		private $container;
		
		/**
		 * @var Request
		 */
		private $request;
		
		/**
		 * @var Response
		 */
		private $response;
		
		/**
		 * @var Router
		 */
		private $router;
		
		/**
		 * @var Connection
		 */
		private $db;
		
		/**
		 * Application constructor.
		 * @param Container $container
		 * @throws \ReflectionException
		 */
		protected function __construct(
			Container $container
		)
		{
			$config = include_once dirname(__FILE__, 3) . '/config/main.php';
			
			foreach ($config['singletons'] ?? [] as $item) {
				$container->bindSingleton($item, $container->get($item));
			}
			
			foreach($config['map'] ?? [] as $contract => $implementation) {
				$container->bindImplementation($contract, $implementation);
			}
			
			$this->router = $container->get(Router::class);
			$this->request = $container->get(Request::class);
			$this->response = $container->get(Response::class);
			$this->db = $container->get(Connection::class);
			
			$this->container = $container;
		}
		
		/**
		 * @throws \Exception
		 */
		public function run()
		{
			try {
				$callback = $this->request->getRoute();
			} catch (HttpException $e) {
				die('Error ' . $e->getStatus() . '. ' . $e->getMessage() . '.');
			}
			
			try {
				$data = \call_user_func_array($callback, $this->request->get());
			} catch (ArgumentCountError $e) {
				throw new HttpException(400, 'Wrong request: too few parameters');
			}
			
			echo $data;
			exit;
		}
		
		/**
		 * @return Application
		 */
		public static function getInstance(): Application
		{
			if (static::$instance === null) {
				try {
					static::$instance = new static(
						new Container
					);
				} catch (\ReflectionException $e) {
					die('Internal error');
				}
			}
			
			return static::$instance;
		}
		
		/**
		 * @return Container
		 */
		public function getContainer(): Container
		{
			return $this->container;
		}
		
		/**
		 * @return Router
		 */
		public function getRouter(): Router
		{
			return $this->router;
		}
		
		/**
		 * @return Request
		 */
		public function getRequest(): Request
		{
			return $this->request;
		}
		
		/**
		 * @return Response
		 */
		public function getResponse(): Response
		{
			return $this->response;
		}
		
		/**
		 * @return Connection
		 */
		public function getDb(): Connection
		{
			return $this->db;
		}
	}