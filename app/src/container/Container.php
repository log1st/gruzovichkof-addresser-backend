<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 18.08.2018
	 * Time: 13:10
	 */
	
	namespace App\Src\Container\Contracts;
	
	use App\Src\Http\Request;
	use App\Src\Http\Router;
	
	class Container
	{
		/**
		 * @var array
		 */
		protected $map = [];
		
		/**
		 * @var array
		 */
		protected $instances = [];
		
		public function __construct()
		{
			$this->instances[Container::class] = $this;
		}
		
		/**
		 * @param string $contract
		 * @param $implementation
		 * @return Container
		 */
		public function bindImplementation(string $contract, $implementation) : Container {
			$this->map[$contract] = $implementation;
			
			return $this;
		}
		
		/**
		 * @param string $contract
		 * @param $target
		 * @return Container
		 */
		public function bindSingleton(string $contract, $target) : Container {
			$this->instances[$contract] = $target;
			
			return $this;
		}
		
		/**
		 * @param $class
		 * @param array $arguments
		 * @return mixed
		 * @throws \ReflectionException
		 * @throws \Exception
		 */
		public function get($class, array $arguments = [])
		{
			$item = null;
			
			if(array_key_exists($class, $this->instances)) {
				return $this->instances[$class];
			}
			
			if (!array_key_exists($class, $this->map)) {
				$item = $class;
			} else {
				$item = $this->map[$class];
			}
			
			if(\is_object($item)) {
				return $item;
			}
			
			$ref = new \ReflectionClass($item);
			
			$constructor = $ref->getConstructor();
			
			$instances = [];
			if($constructor) {
				$dependencies = $constructor->getParameters();
				
				$parameters = $this->keyParametersByArgument(
					$dependencies, $arguments
				);
				
				$instances = $this->getDependencies(
					$dependencies, $parameters
				);
			}
			
			return $ref->newInstanceArgs($instances);
		}
		
		/**
		 * @param array $dependencies
		 * @param array $parameters
		 * @return array
		 */
		protected function keyParametersByArgument(array $dependencies, array $parameters): array
		{
			foreach ($parameters as $key => $value) {
				if (is_numeric($key)) {
					unset($parameters[$key]);
					
					$parameters[$dependencies[$key]->name] = $value;
				}
			}
			
			return $parameters;
		}
		
		/**
		 * @param $parameters
		 * @param array $primitives
		 * @return array
		 * @throws \RuntimeException
		 * @throws \Exception
		 */
		protected function getDependencies($parameters, array $primitives = array()): array
		{
			$dependencies = [];
			
			/** @var \ReflectionParameter $parameter */
			/** @var array $parameters */
			foreach ($parameters as $parameter) {
				$dependency = $parameter->getClass();
				
				
				if (array_key_exists($parameter->name, $primitives)) {
					$dependencies[] = $primitives[$parameter->name];
				} elseif (null === $dependency) {
					if ($parameter->isDefaultValueAvailable()) {
						$dependencies[] = $parameter->getDefaultValue();
					}   else {
						$dependencies[] = null;
					}
				} else {
					$dependencies[] = $this->get($parameter->getClass()->getName());
				}
			}
			
			return $dependencies;
		}
	}