<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 14:24
	 */
	
	namespace App\Src\Db;
	
	use PDO;
	
	class Connection
	{
		/**
		 * @var PDO
		 */
		protected $pdo = null;
		
		/**
		 * Connection constructor.
		 */
		public function __construct()
		{
			$host = env('DB_HOST');
			$name = env('DB_NAME');
			$user = env('DB_USER');
			$password = env('DB_PASSWORD');
			
			$connection = new PDO("mysql:host=$host;dbname=$name;charset=utf8", $user, $password);
			
			$this->pdo = $connection;
		}
		
		/**
		 * @param string $command
		 * @param array $bindings
		 * @return mixed
		 */
		public function rawCommand(string $command, array $bindings = []) {
			$command = $this->pdo->prepare($command);
			
			foreach($bindings as $key => $value) {
				$command->bindValue($key, $value);
			}
			
			$command->execute();
			
			return $command->fetch();
		}
		
		public function insertIntoTable(string $table, array $attributes = []) {
			$keys = implode(',', array_keys($attributes));
			$valuesMap = implode(',', array_fill(0, count($attributes), '?'));
			
			$command = $this->pdo->prepare("INSERT INTO $table ($keys) VALUES ($valuesMap)");
			$command->execute(array_values($attributes));
		}
	}