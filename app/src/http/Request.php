<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:45
	 */
	
	namespace App\Src\Http;
	
	
	class Request
	{
		/**
		 * @var Router
		 */
		private $router;
		
		/**
		 * Request constructor.
		 * @param Router $router
		 */
		public function __construct(Router $router)
		{
			$this->router = $router;
		}
		
		/**
		 * @param $item
		 * @param null $default
		 * @return null
		 */
		public function get($item = null, $default = null) {
			return $item === null ? $_GET : ($_GET[$item] ?? $default ?? null);
		}
		
		/**
		 * @param $item
		 * @param null $default
		 * @return null
		 */
		public function post($item = null, $default = null) {
			return $item === null ? $_POST : ($_POST[$item] ?? $default ?? null);
		}
		
		/**
		 * @return mixed
		 * @throws \Exception
		 */
		public function getRoute()
		{
			$route = $this->get('route');
			unset($_GET['route'], $_REQUEST['route']);

			return $this->router->resolve($route, $_SERVER['REQUEST_METHOD']);
		}
		
		public function router() {
			return $this->router;
		}
	}