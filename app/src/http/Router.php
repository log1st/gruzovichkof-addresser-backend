<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:48
	 */
	
	namespace App\Src\Http;
	
	
	use App\Src\Application\Application;
	use App\Src\Http\Exceptions\HttpException;
	use App\Src\Http\Exceptions\UnknownRouteException;
	use RecursiveDirectoryIterator;
	use RecursiveIteratorIterator;
	
	class Router
	{
		const TYPE_GET = 1;
		const TYPE_POST = 2;
		
		const ROUTES_FILE_NAME = 'routes.php';
		
		protected $routes;
		
		protected function findAllRoutes()
		{
			$it = new RecursiveDirectoryIterator(\dirname(__DIR__, 2));
			foreach (new RecursiveIteratorIterator($it) as $file) {
				$parts = explode(DIRECTORY_SEPARATOR, $file);
				if (array_pop($parts) === self::ROUTES_FILE_NAME) {
					include $file;
				}
			}
		}
		
		protected function add(string $type, array $data): Router
		{
			$this->routes[] = array_merge([
				'type' => $type
			], $data);
			
			return $this;
		}
		
		public function get($path, $alias, $callback): Router
		{
			$this->add('get', [
				'path' => $path,
				'alias' => $alias,
				'callback' => $callback
			]);
			
			return $this;
		}
		
		public function post($path, $alias, $callback): Router
		{
			$this->add('post', [
				'path' => $path,
				'alias' => $alias,
				'callback' => $callback
			]);
			
			return $this;
		}
		
		/**
		 * @param $alias
		 * @param array $params
		 * @return string
		 * @throws UnknownRouteException
		 */
		public function link($alias, array $params = [])
		{
			foreach ($this->routes as $route) {
				if ($route['alias'] === $alias) {
					break;
				}
			}
			
			if(!isset($route)) {
				throw new UnknownRouteException(sprintf('Unable to find route by alias %s', $alias));
			}
			
			$link = $route['alias'];
			foreach ($params as $key => $value) {
				$pattern = '/\{' . preg_quote($key, '\\') . '\}/';
				if (preg_match_all($pattern, $link)) {
					$link = preg_replace($pattern, $value, $link);
					unset($params[$key]);
				}
			}
			
			return $link . (count($params) ? ('?' . http_build_query($params)) : '');
		}
		
		/**
		 * @param $path
		 * @param $type
		 * @return mixed
		 * @throws \Exception
		 */
		public function resolve($path, $type)
		{
			if ($this->routes === null) {
				$this->findAllRoutes();
			}
			
			$filtered = [];
			
			
			foreach ((array)$this->routes as $route) {
				if (mb_strtolower($route['type']) === mb_strtolower($type)) {
					$filtered[] = $route;
				}
			}
			
			foreach ($filtered as $route) {
				if (preg_match('/' . preg_quote($route['path'], '/') . '/', $path)) {
					$callback = $route['callback'];
					
					if (\is_string($callback) && preg_match('/(.*)@(.*)/', $callback, $matches)) {
						$class = $matches[1];
						
						$object = Application::getInstance()->getContainer()->get($class);
						
						$callback = [$object, $matches[2]];
					}
					
					return $callback;
				}
			}
			
			throw new HttpException(404, 'Route not found');
		}
	}