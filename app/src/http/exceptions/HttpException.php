<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:51
	 */
	
	namespace App\Src\Http\Exceptions;
	
	
	use Throwable;
	
	class HttpException extends \Exception
	{
		
		protected $status;
		
		public function __construct($status, string $message = '', int $code = 0, Throwable $previous = null)
		{
			$this->status = $status;
			parent::__construct($message, $code, $previous);
		}
		
		public function getStatus() {
			return $this->status;
		}
	}