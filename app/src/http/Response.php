<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:49
	 */
	
	namespace App\Src\Http;
	
	
	class Response
	{
		const FORMAT_JSON = 1;
		const FORMAT_HTML = 2;
		
		protected $format = self::FORMAT_JSON;
		
		protected $status = 200;
		private $content = '';
		
		public function __toString()
		{
			header('Content-type: ' . $this->getContentType());
			return $this->content;
		}
		
		protected function getContentType() {
			return [
					self::FORMAT_JSON => 'application/json',
					self::FORMAT_HTML => 'text/html'
				][$this->format] ?? 'text/html';
		}
		
		public function setFormat(int $format) : Response {
			$this->format = $format;
			
			return $this;
		}
		
		public function json($status, $body) : Response {
			$this->status = $status;
			$this->content = json_encode([
				'status' => $status,
				'body' => $body
			]);
			
			return $this;
		}
	}