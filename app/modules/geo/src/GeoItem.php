<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 19:03
	 */
	
	namespace App\Modules\Geo\Src;
	
	
	use App\Modules\Geo\Src\Contracts\GeoItemContract;
	
	class GeoItem implements GeoItemContract
	{
		protected $meta = [];
		
		/**
		 * @return string
		 */
		public function getType(): string
		{
			return $this->meta['type'] ?? null;
		}
		
		/**
		 * @return string
		 */
		public function getValue(): string
		{
			return $this->meta['value'] ?? null;
		}
		
		/**
		 * @param string $type
		 * @return GeoItemContract
		 */
		public function setType(string $type): GeoItemContract
		{
			$this->meta['type'] = $type;
			
			return $this;
		}
		
		/**
		 * @param string $value
		 * @return GeoItemContract
		 */
		public function setValue(string $value): GeoItemContract
		{
			$this->meta['value'] = $value;
			
			return $this;
		}
		
		/**
		 */
		public function jsonSerialize()
		{
			return $this->meta;
		}
	}