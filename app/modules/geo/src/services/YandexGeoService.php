<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 16:43
	 */
	
	namespace App\Modules\Geo\Src\Services;
	
	
	use App\Modules\Geo\Src\Contracts\GeoItemContract;
	use App\Modules\Geo\Src\Contracts\GeoItemsRepositoryContract;
	use App\Modules\Geo\Src\Contracts\GeoServiceContract;
	use App\Modules\Geo\Src\GeoItem;
	use App\Src\Container\Contracts\Container;
	use GuzzleHttp\Client;
	use GuzzleHttp\Message\Request;
	
	class YandexGeoService implements GeoServiceContract
	{
		const API_URL = 'https://geocode-maps.yandex.ru/1.x/';
		
		const LIMIT = 6;
		/**
		 * @var Client
		 */
		private $http;
		/**
		 * @var Container
		 */
		private $container;
		
		/**
		 * YandexGeoService constructor.
		 * @param Client $http
		 * @param Container $container
		 */
		public function __construct(
			Client $http,
			Container $container
		)
		{
			$this->http = $http;
			$this->container = $container;
		}
		
		public function type(): string
		{
			return 'yandex';
		}
		
		public function title(): string
		{
			return 'Яндекс.Карты';
		}
		
		/**
		 * @param string $query
		 * @return GeoItemsRepositoryContract
		 * @throws \ReflectionException
		 */
		public function getAddressesByQuery(string $query): GeoItemsRepositoryContract
		{
			/** @var Request $request */
			$request = $this->http->get(static::API_URL . '?' . http_build_query([
					'geocode' => $query,
					'results' => static::LIMIT,
					'format' => 'json',
//					'lang' => 'en_US'
				]));
			
			$body = $request->getBody();
			
			$body = json_decode($body, true);
			
			/** @var GeoItemsRepositoryContract $repository */
			$repository = $this->container->get(GeoItemsRepositoryContract::class);
			
			foreach ($body['response']['GeoObjectCollection']['featureMember'] as $item) {
				$item = $item['GeoObject']['metaDataProperty']['GeocoderMetaData'];
				/** @var GeoItemContract $item */
				$geoItem = $this->container->get(GeoItemContract::class);
				$repository->add(
					$geoItem
						->setType($item['kind'])
						->setValue($item['Address']['formatted'])
				);
			}
			
			return $repository;
		}
	}