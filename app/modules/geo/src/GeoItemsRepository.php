<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 18:40
	 */
	
	namespace App\Modules\Geo\Src;
	
	
	use App\Modules\Geo\Src\Contracts\GeoItemContract;
	use App\Modules\Geo\Src\Contracts\GeoItemsRepositoryContract;
	
	class GeoItemsRepository implements GeoItemsRepositoryContract
	{
		/**
		 * @var GeoItemContract[]
		 */
		protected $items = [];
		protected $position = 0;
		
		public function next()
		{
			$this->position++;
		}
		
		public function key()
		{
			return $this->position;
		}
		
		public function valid()
		{
			return $this->items[$this->position] ?? null !== null;
		}
		
		public function rewind()
		{
			$this->position = 0;
		}
		
		public function count()
		{
			return \count($this->items);
		}
		
		public function add(GeoItemContract ...$item): GeoItemsRepositoryContract
		{
			foreach($item as $i) {
				$this->items[] = $i;
			}
			
			return $this;
		}
		
		public function current(): GeoItemContract
		{
			return $this->items[$this->position];
		}
		
		public function jsonSerialize()
		{
			return $this->items;
		}
	}