<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 16:22
	 */
	
	namespace App\Modules\Geo\Src\Contracts;
	
	
	interface GeoServiceContract
	{
		public function type() : string;
		
		public function title() : string;
		
		public function getAddressesByQuery(string $query) : GeoItemsRepositoryContract;
	}