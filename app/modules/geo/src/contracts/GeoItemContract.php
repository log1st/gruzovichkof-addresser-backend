<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 16:25
	 */
	
	namespace App\Modules\Geo\Src\Contracts;
	
	
	interface GeoItemContract extends \JsonSerializable
	{
		/**
		 * @return string
		 */
		public function getType(): string;
		
		/**
		 * @return string
		 */
		public function getValue(): string;
		
		/**
		 * @param string $type
		 * @return GeoItemContract
		 */
		public function setType(string $type): GeoItemContract;
		
		/**
		 * @param string $value
		 * @return GeoItemContract
		 */
		public function setValue(string $value): GeoItemContract;
	}