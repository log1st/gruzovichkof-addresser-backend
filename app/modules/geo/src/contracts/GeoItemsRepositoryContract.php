<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 16:45
	 */
	
	namespace App\Modules\Geo\Src\Contracts;
	
	
	interface GeoItemsRepositoryContract extends \Countable, \Iterator, \JsonSerializable
	{
		public function add(GeoItemContract ...$item) : GeoItemsRepositoryContract;
		
		public function current() : GeoItemContract;
	}