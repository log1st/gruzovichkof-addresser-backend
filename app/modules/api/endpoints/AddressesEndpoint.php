<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:43
	 */
	
	namespace App\Modules\Api\Endpoints;
	
	
	use App\Modules\Api\Abstraction\AbstractEndpoint;
	use App\Modules\Geo\Src\Contracts\GeoServiceContract;
	use App\Src\Db\Connection;
	use App\Src\Http\Exceptions\HttpException;
	use App\Src\Http\Response;
	
	class AddressesEndpoint extends AbstractEndpoint
	{
		/**
		 * @var GeoServiceContract
		 */
		private $geoService;
		/**
		 * @var Response
		 */
		private $response;
		/**
		 * @var Connection
		 */
		private $db;
		
		/**
		 * AddressesEndpoint constructor.
		 * @param GeoServiceContract $geoService
		 * @param Response $response
		 * @param Connection $db
		 */
		public function __construct(
			GeoServiceContract $geoService,
			Response $response,
			Connection $db
		)
		{
			$this->geoService = $geoService;
			$this->response = $response;
			$this->db = $db;
		}
		
		/**
		 * @param string $q
		 * @return Response
		 */
		public function run(string $q)
		{
			if (mb_strlen($q) < 3) {
				return $this->response->json(400, [
					'message' => 'Too short query'
				]);
			}
			
			$repository = $this->geoService->getAddressesByQuery($q);
			
			$this->db->insertIntoTable('requests', [
				'query' => $q,
				'response' => json_encode($repository, JSON_UNESCAPED_UNICODE),
				'date' => time()
			]);
			
			return $this->response->json(200, [
				'items' => json_decode(json_encode($repository), true)
			]);
		}
	}