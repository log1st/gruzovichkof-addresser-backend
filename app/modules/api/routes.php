<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 12:53
	 */
	
	$router = \App\Src\Application\Application::getInstance()->getRouter();
	
	$router->get(
		'api/addresses',
		'api.addresses.list',
		\App\Modules\Api\Endpoints\AddressesEndpoint::class . '@run'
	);
