<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 19.08.2018
	 * Time: 14:33
	 */
	
	include dirname(__FILE__, 2) . '/run/command_bootstrap.php';
	
	$db = \App\Src\Application\Application::getInstance()->getDb();
	
	$db->rawCommand('DROP TABLE IF EXISTS requests');
	$db->rawCommand('CREATE TABLE requests (`id` int(11) NOT NULL AUTO_INCREMENT, `query` TEXT, `response` TEXT, `date` INT(11), PRIMARY KEY (id))');