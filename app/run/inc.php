<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 18.08.2018
	 * Time: 13:06
	 */
	
	
	/**
	 * @param $item
	 * @param null $default
	 * @return null
	 */
	function env($item, $default = null)
	{
		$env = getenv($item);
		
		if (in_array(mb_strtolower($env), ['true'])) {
			$env = true;
		}
		
		return $env ?: $default;
	}