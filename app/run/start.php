<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 18.08.2018
	 * Time: 13:07
	 */
	
	mb_internal_encoding('UTF-8');
	
	require dirname(__DIR__) . '/../vendor/autoload.php';
	
	try {
		(new \Dotenv\Dotenv(dirname(__DIR__, 2)))->load();
	} catch (\Dotenv\Exception\InvalidPathException $e) {
	
	}
	
	require_once __DIR__ . '/inc.php';
	
	if (env('DEBUG')) {
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: *');
		header('Access-Control-Allow-Headers: *');
	}