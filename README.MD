#Init:
```bash
composer install
```

#After init
```bash
php app/commands/migrate.php
```

#Environments:
```DB_HOST```: Database host

```DB_NAME```: Database name

```DB_USER```: Database user

```DB_PASSWORD```: Database password