<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 18.08.2018
	 * Time: 12:59
	 */
	
	require_once '../app/run/start.php';
	
	$application = \App\Src\Application\Application::getInstance();
	
	try {
		$application->run();
	} catch (\App\Src\Http\Exceptions\HttpException $e) {
		die('Error ' . $e->getStatus() . '. ' . $e->getMessage() . '.');
	} catch (Exception $e) {
		if(env('DEBUG')) {
			throw $e;
		}
		die('Internal error while running application');
	}